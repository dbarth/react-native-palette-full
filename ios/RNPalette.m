
#import "RNPalette.h"
#import "UIPalette.h"

@interface RNPalette ()
@end


@implementation RNPalette

@synthesize bridge = _bridge;

RCT_EXPORT_MODULE();


RCT_REMAP_METHOD(getNamedSwatches,
                 getNamedSwatchesWithUrl: (NSString *)url
                 resolver:(RCTPromiseResolveBlock)resolve
                 rejecter:(RCTPromiseRejectBlock)reject)
{
  
  
  //NSURL * urlImage = [NSURL URLWithString: url];
  UIImage * image = [UIImage imageWithContentsOfFile:url];
  [self getNamedSwatchesFromImage:image resolver:resolve rejecter:reject];
  
}



RCT_REMAP_METHOD(getAllSwatches,
                 getAllSwatchesWithUrl: (NSString *)url
                 resolver:(RCTPromiseResolveBlock)resolve
                 rejecter:(RCTPromiseRejectBlock)reject)
{
  
  
  //NSURL * urlImage = [NSURL URLWithString: url];
  UIImage * image = [UIImage imageWithContentsOfFile:url];
  [self getAllSwatchesFromImage:image resolver:resolve rejecter:reject];
  
}


- (void)getAllSwatchesFromImage:(UIImage *)image
                         resolver:(RCTPromiseResolveBlock)resolve
                         rejecter:(RCTPromiseRejectBlock)reject
{
  
  [image getPaletteImageColorWithMode:ALL_MODE_PALETTE withCallBack:^(PaletteColorModel *recommendColor, NSDictionary *allModeColorDic,NSError *error) {
    
    
   if (error) {
      reject(@"500", @"Error analize", error);
     return;
    }
    
    
    NSMutableArray * data = [[NSMutableArray alloc] init];
    
    if (recommendColor!=nil){
      [data addObject:@{ @"color":[recommendColor imageColorString]}];
    }
    if ([[allModeColorDic objectForKey:@"vibrant"] isKindOfClass:[PaletteColorModel class]]){
      PaletteColorModel * vibrant = (PaletteColorModel *)[allModeColorDic objectForKey:@"vibrant"];
      [data addObject:@{ @"color":[vibrant imageColorString],
                         @"percentage":@([vibrant percentage])}];
    }
    if ([[allModeColorDic objectForKey:@"dark_vibrant"] isKindOfClass:[PaletteColorModel class]]){
      PaletteColorModel * dark_vibrant = (PaletteColorModel *)[allModeColorDic objectForKey:@"dark_vibrant"];
      [data addObject:@{ @"color":[dark_vibrant imageColorString],
                         @"percentage":@([dark_vibrant percentage])}];
    }
    if ([[allModeColorDic objectForKey:@"light_vibrant"] isKindOfClass:[PaletteColorModel class]]){
      PaletteColorModel * light_vibrant = (PaletteColorModel *)[allModeColorDic objectForKey:@"light_vibrant"];
      [data addObject:@{ @"color":[light_vibrant imageColorString],
                         @"percentage":@([light_vibrant percentage])}];
    }
    if ([[allModeColorDic objectForKey:@"muted"] isKindOfClass:[PaletteColorModel class]]){
      PaletteColorModel * muted = (PaletteColorModel *)[allModeColorDic objectForKey:@"muted"];
      [data addObject:@{ @"color":[muted imageColorString],
                         @"percentage":@([muted percentage])}];
    }
    if ([[allModeColorDic objectForKey:@"dark_muted"] isKindOfClass:[PaletteColorModel class]]){
      PaletteColorModel * dark_muted = (PaletteColorModel *)[allModeColorDic objectForKey:@"dark_muted"];
        [data addObject:@{ @"color":[dark_muted imageColorString],
                           @"percentage":@([dark_muted percentage])}];
    }
    if ([[allModeColorDic objectForKey:@"light_muted"] isKindOfClass:[PaletteColorModel class]]){
      PaletteColorModel * light_muted = (PaletteColorModel *)[allModeColorDic objectForKey:@"light_muted"];
        [data addObject:@{ @"color":[light_muted imageColorString],
                           @"percentage":@([light_muted percentage])}];
    }

    
    
    resolve(data);
  }];

  
}


- (void)getNamedSwatchesFromImage:(UIImage *)image
                        resolver:(RCTPromiseResolveBlock)resolve
                        rejecter:(RCTPromiseRejectBlock)reject
{
  
  [image getPaletteImageColorWithMode:ALL_MODE_PALETTE withCallBack:^(PaletteColorModel *recommendColor, NSDictionary *allModeColorDic,NSError *error) {
    

    if (error) {
      reject(@"500", @"Error analize", error);
      return;
    }
    
    NSMutableDictionary * data = [[NSMutableDictionary alloc] init];
  
    if ([[allModeColorDic objectForKey:@"vibrant"] isKindOfClass:[PaletteColorModel class]]){
      PaletteColorModel * vibrant = (PaletteColorModel *)[allModeColorDic objectForKey:@"vibrant"];
      [data setObject:@{ @"color":[vibrant imageColorString],
                         @"percentage":@([vibrant percentage])} forKey:@"Vibrant"];
    }
    if ([[allModeColorDic objectForKey:@"dark_vibrant"] isKindOfClass:[PaletteColorModel class]]){
      PaletteColorModel * dark_vibrant = (PaletteColorModel *)[allModeColorDic objectForKey:@"dark_vibrant"];
      [data setObject:@{ @"color":[dark_vibrant imageColorString],
                         @"percentage":@([dark_vibrant percentage])} forKey:@"Vibrant Dark"];
    }
    if ([[allModeColorDic objectForKey:@"light_vibrant"] isKindOfClass:[PaletteColorModel class]]){
      PaletteColorModel * light_vibrant = (PaletteColorModel *)[allModeColorDic objectForKey:@"light_vibrant"];
      [data setObject:@{ @"color":[light_vibrant imageColorString],
                         @"percentage":@([light_vibrant percentage])} forKey:@"Vibrant Light"];
    }
    if ([[allModeColorDic objectForKey:@"muted"] isKindOfClass:[PaletteColorModel class]]){
      PaletteColorModel * muted = (PaletteColorModel *)[allModeColorDic objectForKey:@"muted"];
      [data setObject:@{ @"color":[muted imageColorString],
                         @"percentage":@([muted percentage])} forKey:@"Muted"];
    }
    if ([[allModeColorDic objectForKey:@"dark_muted"] isKindOfClass:[PaletteColorModel class]]){
      PaletteColorModel * dark_muted = (PaletteColorModel *)[allModeColorDic objectForKey:@"dark_muted"];
      [data setObject:@{ @"color":[dark_muted imageColorString],
                         @"percentage":@([dark_muted percentage])} forKey:@"Muted Dark"];
    }
    if ([[allModeColorDic objectForKey:@"light_muted"] isKindOfClass:[PaletteColorModel class]]){
      PaletteColorModel * light_muted = (PaletteColorModel *)[allModeColorDic objectForKey:@"light_muted"];
      [data setObject:@{ @"color":[light_muted imageColorString],
                         @"percentage":@([light_muted percentage])} forKey:@"Muted Light"];
    }

   
    
    resolve(data);
    
    
  }];

}

@end
